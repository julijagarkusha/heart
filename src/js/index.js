import inputShow from "./utils/inputShow";
import inputHidden from "./utils/inputHidden";
import popupShow from './utils/popupShow';
import popUpClose from "./utils/popUpClose";
import menuClick from "./utils/menuClick";
import formToggle from "./utils/formToggle";

sessionStorage.setItem('searchShow', 'hidden');

const inputShowButton = document.querySelector('.search--show');
inputShowButton.addEventListener('click', inputShow);

const bodyElement = document.querySelector('body');
bodyElement.addEventListener('click', inputHidden);

const icons = document.querySelectorAll(".customLink--show");
icons.forEach(icon => {
  icon.addEventListener("click", popupShow);
});

const closeIcons = document.querySelectorAll('.popUpClose');
closeIcons.forEach(closeIcon => {
  closeIcon.addEventListener("click", popUpClose);
});

const menuItems = document.querySelectorAll(".menuItem");
menuItems.forEach(menuItem => {
  menuItem.addEventListener("click", menuClick)
});

const toFormElement = document.querySelector(".customLink--quickly");
toFormElement.addEventListener("click", popupShow);

const formButtons = document.querySelectorAll(".usersForm__toggleButton");
formButtons.forEach(formButton => {
  formButton.addEventListener("click", formToggle)
});


