import navToggle from "./projectNavToggle";

const drawProjects = (projects, cb=()=>{}) => {

  projects.forEach(project => {
    fetch("/includes/projectCard.html").then(res => {
      return res.text();
    }).then(data => {
      const cardElement = document.createElement('article');
      document.querySelector(".projectList").appendChild(cardElement);
      cardElement.innerHTML = data;

      cardElement.id = project.id;
      cardElement.classList.add("projectCart");
      cardElement.classList.add("projectCart--small");
      cardElement.querySelector("img").src = project.src || "/assets/images/projects/no-photo.jpg";
      cardElement.querySelector("img").alt = project.id;
      cardElement.querySelector("h3").innerHTML = project.name;
      cardElement.querySelector('.projectCart__description').innerHTML = project.description;
      cardElement.querySelector('.projectCart__date').innerHTML = project.date;
      cardElement.querySelector('.projectCart__totalSum').innerHTML = project.totalSum;
      cardElement.querySelector('.projectCart__countSum').innerHTML = project.countSum;
      cb();
    }).catch(exception => {
      console.log('exception', exception);
    });
  });
  document.querySelector('footer').style.display = "none";
  const navItems = document.querySelectorAll(".projects__navItem a");
  navItems.forEach(navItem => {
    navItem.addEventListener("click", navToggle);
  });
};

export default drawProjects;
 