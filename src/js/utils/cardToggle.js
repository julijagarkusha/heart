const classToggle = (event) => {
  const target = event.target;
  console.log(target.classList.value);
  const cards = document.querySelectorAll(".projectCart");
  if (target.classList.value !== "projectCart__helpItem" && target.classList.value !== "helpLink") {
    setTimeout(() => {
      cards.forEach(card => {
        card.classList.value = "projectCart projectCart--small";
      });
      target.classList.value = "projectCart projectCart--big";
    }, 200);
  }
};

export default classToggle
