const tabsToggle = (event) => {
  const target = event.target;
  const tabs = document.querySelectorAll(".tab__item");
  const tabsSwitchElem = document.querySelector(".switchElement");
  tabs.forEach(tab => {
    tab.classList.remove("tab__item--active");
  });
  if (target.classList.contains("tab__item")) {
    target.classList.add("tab__item--active")
  } else if (target.parentNode.classList.value === "tab__item") {
    target.parentNode.classList.add("tab__item--active")
  } else {
    target.parentNode.parentNode.classList.add("tab__item--active")
  }

  if(document.querySelector("#make").classList.contains("tab__item--active")) {
    tabsSwitchElem.classList.value = "switchElement switchElement__makeActive";
  } else if(document.querySelector("#financial").classList.contains("tab__item--active")) {
    tabsSwitchElem.classList.value = "switchElement switchElement__financialActive";
  } else if(document.querySelector("#material").classList.contains("tab__item--active")) {
    tabsSwitchElem.classList.value = "switchElement switchElement__materialActive"
  } else {
    tabsSwitchElem.classList.value = "switchElement switchElement__volunteeringActive"
  }
};
export default tabsToggle;