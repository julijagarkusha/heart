const cardBigAdd = () => {
  const projectList = document.querySelector(".projectList");
  projectList.firstChild.classList.remove("projectCart--small");
  projectList.firstChild.classList.add("projectCart--big");
};

export default cardBigAdd;
