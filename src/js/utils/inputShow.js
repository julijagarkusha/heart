const inputShow = (event) => {
  const target = event.target;
  document.querySelector('.search__inputWrapper').classList.remove("search__inputWrapper--hidden");
  document.querySelector('.search__inputWrapper').classList.toggle("search__inputWrapper--visible");
  sessionStorage.setItem('searchShow', 'show');
};

export default inputShow;