const inputHidden = (event) => {
  const target = event.target;
  if (target.classList.value.indexOf('customInput') === (-1) &&
      target.classList.value.indexOf('search--show') === (-1) &&
      target.classList.value.indexOf('searchShow') === (-1)){
    document.querySelector('.search__inputWrapper').classList.add("search__inputWrapper--hidden");
    setTimeout(()=> {
      document.querySelector('.search__inputWrapper').classList.remove("search__inputWrapper--visible");
    }, 300);
      sessionStorage.setItem('searchShow', 'hidden');
  } else {

  }
};

export default inputHidden;

