const formToggle = (event) => {
  event.preventDefault();
  const target = event.target;
  const buttons = document.querySelectorAll(".usersForm__toggleButton");
  const formContainer = document.querySelector(".usersForm__info");
  buttons.forEach(button => {
    button.classList.remove("usersForm__toggleButton--active");
  });
  target.classList.add("usersForm__toggleButton--active");
  formContainer.innerHTML = "";
  if(target.value === "individual") {
    fetch("/includes/usersForm.html").then(res => {
      return res.text();
    }).then(data => {
      setTimeout(()=>{
        formContainer.innerHTML = data;
      },200);
    }).catch(exception => {
      console.log('exception', exception);
    })
  } else {
    fetch("/includes/entityForm.html").then(res => {
      return res.text();
    }).then(data => {
      setTimeout(()=>{
        formContainer.innerHTML = data;
      },200);
    }).catch(exception => {
      console.log('exception', exception);
    })
  }
};

export default formToggle;