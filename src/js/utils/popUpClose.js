const popUpClose = () => {
  const bodyElement = document.querySelector("body");
  const popupElements = document.querySelectorAll(".popUp");
  popupElements.forEach(popupElement => {
    popupElement.classList.remove("popUp--show");
  });
  bodyElement.classList.remove('body--blur');
};
export default popUpClose;
