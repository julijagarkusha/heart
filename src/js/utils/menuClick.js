import drawProjects from "./drawProjects";
import projects from "../projects";
import cardClick from "../utils/cardClick";

const currentProjects = projects.filter(project => {
  return !project.archive && project.category === "children"
});

const archiveProjects = projects.filter(project => {
  return project.archive === true && project.category === "children"
});

const urgentlyProjects = projects.filter(project => {
  return project.urgently === true && project.category === "children"
});

const menuClick = (event) => {
  event.preventDefault();
  const target = event.target;
  const menuItems = document.querySelectorAll(".menuLink");
  let pageURL = "";

  menuItems.forEach(menuItem => {
    menuItem.classList.remove("menuLink--active");
  });

  if (target.getAttribute("data-href") !== null) {
    pageURL = target.getAttribute("data-href");
    target.classList.add("menuLink--active");
  } else {
    pageURL = target.firstChild.getAttribute("data-href");
    target.firstChild.classList.add("menuLink--active");
  }

  fetch(pageURL).then((response) => {
    return response.text();
    }).then(data => {
     document.querySelector("main").innerHTML = "";
     document.querySelector("main").innerHTML = data;
    const bodyElement = document.querySelector("body");
    bodyElement.classList.remove("body--urgently");
     switch (pageURL) {
       case "/includes/projects.html" :
         setTimeout(()=> {
           drawProjects(currentProjects, () => {
             cardClick();
           });
         }, 200);
         break;
       case "/includes/arhive.html" :
         setTimeout(()=> {
           drawProjects(archiveProjects, () => {
             cardClick();
           });
         }, 200);
         break;
       case "/includes/yourHelp.html" :
         bodyElement.classList.add("body--urgently");
         setTimeout(() => {
           drawProjects(urgentlyProjects, () => {
             cardClick();
           });
         }, 200);
         break;
       case "/includes/contacts.html" :
         document.querySelector("footer").classList.add("contentInfo__contacts")
     }
  });
};

export default menuClick;
