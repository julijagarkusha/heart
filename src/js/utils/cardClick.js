import cardBigAdd from "./cardBigAdd";
import cardToggle from "./cardToggle";

const cardClick = () => {
  cardBigAdd();
  const cards = document.querySelectorAll(".projectCart");
  cards.forEach(card => {
    card.addEventListener("click", cardToggle)
  });
};

export default cardClick;