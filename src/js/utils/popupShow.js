import tabsToggle from "./tabsToggle";

const popupShow = (event) => {
  const target = event.target;
  const bodyElement = document.querySelector("body");
  bodyElement.classList.add('body--blur');
  if (target.getAttribute("data-value") === "form-show") {
    document.querySelector(".toHelpForm").classList.add("popUp--show");
    fetch("/includes/usersForm.html").then(res => {
      return res.text();
    }).then(data => {
      document.querySelector(".usersForm__info").innerHTML = data;
    }).catch(exception => {
      console.log(exception);
    });

    const tabs = document.querySelectorAll(".tab__item");
    tabs.forEach(tab => {
      tab.addEventListener("click", tabsToggle)
    });
  } else {
    document.querySelector(".popUp").classList.add("popUp--show")
  }
};
export default popupShow;