import drawProjects from "./drawProjects";
import projects from "../projects";
import cardClick from "./cardClick";

//current projects
const ecologyCurrent = projects.filter(project => {
  return !project.archive && project.category === "ecology"
});

const childrenCurrent = projects.filter(project => {
  return !project.archive && project.category === "children"
});

const warCurrent = projects.filter(project => {
  return !project.archive && project.category === "war"
});

const educationCurrent = projects.filter(project => {
  return !project.archive && project.category === "education"
});

//archive projects
const ecologyArchive = projects.filter(project => {
  return project.archive && project.category === "ecology"
});

const childrenArchive = projects.filter(project => {
  return project.archive && project.category === "children"
});

const warArchive = projects.filter(project => {
  return project.archive && project.category === "war"
});

const educationArchive = projects.filter(project => {
  return project.archive && project.category === "education"
});

// urgent projects
const ecologyUrgent = projects.filter(project => {
  return project.urgently && project.category === "ecology"
});

const childrenUrgent = projects.filter(project => {
  return project.urgently && project.category === "children"
});

const warUrgent = projects.filter(project => {
  return project.urgently && project.category === "war"
});

const educationUrgent = projects.filter(project => {
  return project.urgently && project.category === "education"
});

const navToggle = (event) => {
  const target = event.target;
  const navWrapper = document.querySelector(".projects__nav");
  navWrapper.classList.value = "projects__nav";

  const navItems = document.querySelectorAll(".projects__navItem a");
  navItems.forEach(navItem => {
    navItem.classList.remove("projects__navItem--active");
  });
  target.classList.add("projects__navItem--active");
  document.querySelector(".projectList").innerHTML = "";

  const menuItems = document.querySelectorAll(".menuLink");
  menuItems.forEach(menuItem => {
    if (menuItem.classList.contains("menuLink--active")) {
      const activeLink = menuItem.getAttribute("data-href");
      switch (activeLink) {
        case "/includes/projects.html" :
          switch (target.id) {
            case "war" :
              navWrapper.classList.add("projects__war--active");
              setTimeout(() => {
                drawProjects(warCurrent, () => {
                  cardClick();
                });
              }, 200);
              break;
            case "ecology" :
              navWrapper.classList.add("projects__ecology--active");
              setTimeout(()=> {
                drawProjects(ecologyCurrent, () => {
                  cardClick();
                });
              }, 200);
              break;
            case "children" :
              navWrapper.classList.add("projects__children--active");
              setTimeout(()=> {
                drawProjects(childrenCurrent, () => {
                  cardClick();
                });
              }, 200);
              break;
            case "education" :
              navWrapper.classList.add("projects__education--active");
              setTimeout(()=> {
                drawProjects(educationCurrent, () => {
                  cardClick();
                });
              }, 200);
              break;
          } break;
        case "/includes/arhive.html" :
          switch (target.id) {
            case "war" :
              navWrapper.classList.add("projects__war--active");
              setTimeout(() => {
                drawProjects(warArchive, () => {
                  cardClick();
                });
              }, 200);
              break;
            case "ecology" :
              navWrapper.classList.add("projects__ecology--active");
              setTimeout(()=> {
                drawProjects(ecologyArchive, () => {
                  cardClick();
                });
              }, 200);
              break;
            case "children" :
              navWrapper.classList.add("projects__children--active");
              setTimeout(()=> {
                drawProjects(childrenArchive, () => {
                  cardClick();
                });
              }, 200);
              break;
            case "education" :
              navWrapper.classList.add("projects__education--active");
              setTimeout(()=> {
                drawProjects(educationArchive, () => {
                  cardClick();
                });
              }, 200);
              break;
          } break;
        case "/includes/yourHelp.html" :
          switch (target.id) {
            case "war" :
              navWrapper.classList.add("projects__war--active");
              setTimeout(() => {
                drawProjects(warUrgent, () => {
                  cardClick();
                });
              }, 200);
              break;
            case "ecology" :
              navWrapper.classList.add("projects__ecology--active");
              setTimeout(()=> {
                drawProjects(ecologyUrgent, () => {
                  cardClick();
                });
              }, 200);
              break;
            case "children" :
              navWrapper.classList.add("projects__children--active");
              setTimeout(()=> {
                drawProjects(childrenUrgent, () => {
                  cardClick();
                });
              }, 200);
              break;
            case "education" :
              navWrapper.classList.add("projects__education--active");
              setTimeout(()=> {
                drawProjects(educationUrgent, () => {
                  cardClick();
                });
              }, 200);
              break;
          } break;
      }
    }
  });
};

export default navToggle;